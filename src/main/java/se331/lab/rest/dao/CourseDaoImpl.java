package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.repository.CourseRepository;

import java.util.ArrayList;
import java.util.List;
@Profile("LabDao")
@Repository
@Slf4j
public class CourseDaoImpl implements CourseDao {
    List<Course> courses;
    public CourseDaoImpl(){
        this.courses = new ArrayList<>();
        Lecturer a = new Lecturer();
        a.setName("Pree");
        a.setSurname("Thiengburanathum");
        this.courses.add(Course.builder()
                .id(1l)
                .courseId("953494")
                .courseName("Selected Topics in SE 1")
                .content("Python and Data mining Development")
                .lecturer(a)
                .build());
        this.courses.add(Course.builder()
                .id(2l)
                .courseId("953234")
                .courseName("Advance Software Development")
                .content("The course for the smart students")
                .lecturer(a)
                .build());
    }
    @Override
    public List<Course> getAllCourses() {
        return courses;
    }

    @Override
    public Course findById(Long courseId) {
        return courses.get((int) (courseId -1));
    }
}
